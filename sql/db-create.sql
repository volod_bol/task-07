DROP DATABASE IF EXISTS testdb;

CREATE DATABASE testdb
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United Kingdom.1251'
    LC_CTYPE = 'English_United Kingdom.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

-- \c testdb;

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    login varchar(10) UNIQUE NOT NULL
);

CREATE TABLE teams (
    id SERIAL PRIMARY KEY,
    name varchar(10) UNIQUE NOT NULL
);

CREATE TABLE users_teams (
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    team_id INTEGER REFERENCES teams (id) ON DELETE CASCADE
);

INSERT INTO users VALUES (DEFAULT, 'ivanov');
INSERT INTO teams VALUES (DEFAULT, 'teamA');