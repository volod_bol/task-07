package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
    private Connection connection;

    private DBManager() {
        try (InputStream inputStream = new FileInputStream("app.properties")) {
            Properties properties = new Properties();
            properties.load(inputStream);

            connection = DriverManager.getConnection(properties.getProperty("connection.url"));
        } catch (IOException | SQLException e) {
            System.err.println("Can not connect to DB!");
        }
    }

    private static class DBManagerHelper {
        private static final DBManager INSTANCE = new DBManager();
    }

    public static DBManager getInstance() {
        return DBManagerHelper.INSTANCE;
    }

    public List<User> findAllUsers() throws DBException {
        List<User> userList = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users");
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String login = resultSet.getString("login");
                userList.add(new User(id, login));
            }
        } catch (SQLException e) {
            throw new DBException("Can not find users", e);
        }
        return userList;
    }

    public boolean insertUser(User user) throws DBException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQLQuery.INSERT_USER.query);
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            preparedStatement = connection.prepareStatement(SQLQuery.GET_USER_BY_LOGIN.query);
            preparedStatement.setString(1, user.getLogin());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
                return true;
            }
        } catch (SQLException e) {
            throw new DBException("Can not insert user", e);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public boolean deleteUsers(User... users) throws DBException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.DELETE_USER.query)) {
            for (User user : users) {
                preparedStatement.setString(1, user.getLogin());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DBException("Can not delete that user", e);
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.GET_USER_BY_LOGIN.query)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                return new User(id, login);
            }
        } catch (SQLException e) {
            throw new DBException("Can not find user", e);
        }
        return null;
    }

    public Team getTeam(String name) throws DBException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.GET_TEAM_BY_NAME.query)) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new Team(resultSet.getInt("id"), name);
            }
        } catch (SQLException e) {
            throw new DBException("Can not find team", e);
        }
        return null;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teamList = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM teams");
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                teamList.add(new Team(id, name));
            }
        } catch (SQLException e) {
            throw new DBException("Fatal error during query", e);
        }
        return teamList;
    }

    public boolean insertTeam(Team team) throws DBException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQLQuery.INSERT_TEAM.query);
            preparedStatement.setString(1, team.getName());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            preparedStatement = connection.prepareStatement(SQLQuery.GET_TEAM_BY_NAME.query);
            preparedStatement.setString(1, team.getName());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                team.setId(resultSet.getInt("id"));
                return true;
            }
        } catch (SQLException e) {
            throw new DBException("Fatal error during query", e);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        turnOffAutoCommit();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.INSERT_TEAM_FOR_USER.query)) {
            preparedStatement.setInt(1, user.getId());
            for (Team team : teams) {
                preparedStatement.setInt(2, team.getId());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            rollbackChanges();
            turnOnAutoCommit();
            throw new DBException("Can not set all teams for that user", e);
        }
        commitChanges();
        turnOnAutoCommit();
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teamList = new ArrayList<>();
        try (PreparedStatement getUserTeamsID = connection.prepareStatement(SQLQuery.GET_USER_TEAMS.query);
             PreparedStatement getTeamByID = connection.prepareStatement(SQLQuery.GET_TEAM_BY_ID.query)) {
            getUserTeamsID.setInt(1, user.getId());
            ResultSet teamsIdResult = getUserTeamsID.executeQuery();
            List<Integer> teamsID = new ArrayList<>();
            while (teamsIdResult.next()) {
                teamsID.add(teamsIdResult.getInt("team_id"));
            }
            if (teamsID.isEmpty()) {
                return teamList;
            }
            for (int id : teamsID) {
                getTeamByID.setInt(1, id);
                ResultSet teamFromDB = getTeamByID.executeQuery();
                if (teamFromDB.next()) {
                    String name = teamFromDB.getString("name");
                    teamList.add(new Team(id, name));
                }
            }
        } catch (SQLException e) {
            throw new DBException("Can not get all teams for that user", e);
        }
        return teamList;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.DELETE_TEAM.query)) {
            preparedStatement.setString(1, team.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Can not delete that team", e);
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.UPDATE_TEAM.query)) {
            preparedStatement.setString(1, team.getName());
            preparedStatement.setInt(2, team.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Can't update team", e);
        }
        return true;
    }

    private void turnOnAutoCommit() {
        try {
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void turnOffAutoCommit() {
        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void commitChanges() {
        try {
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void rollbackChanges() {
        try {
            connection.rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    enum SQLQuery {
        INSERT_USER("INSERT INTO users (id, login) VALUES (DEFAULT, ?)"),
        DELETE_USER("DELETE FROM users WHERE login = ?"),
        GET_USER_BY_LOGIN("SELECT * FROM users WHERE login = ?"),
        INSERT_TEAM("INSERT INTO teams (id, name) VALUES (DEFAULT, ?)"),
        DELETE_TEAM("DELETE FROM teams WHERE name = ?"),
        GET_TEAM_BY_NAME("SELECT * FROM teams WHERE name = ?"),
        GET_TEAM_BY_ID("SELECT name FROM teams WHERE id = ?"),
        UPDATE_TEAM("UPDATE teams SET name = ? WHERE id = ?"),
        INSERT_TEAM_FOR_USER("INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)"),
        GET_USER_TEAMS("SELECT team_id FROM users_teams WHERE user_id = ?");

        final String query;

        SQLQuery(String s) {
            this.query = s;
        }
    }
}
